package com.springboot.multiple.datasources;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// causes Lombok to generate a logger field.
@Slf4j
// serves two purposes i.e. configuration and bootstrapping.
@SpringBootApplication
public class Runner {

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
        log.info("Application started successfully.");
    }
}
