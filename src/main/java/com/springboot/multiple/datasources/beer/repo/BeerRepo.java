package com.springboot.multiple.datasources.beer.repo;

import com.springboot.multiple.datasources.model.Beer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BeerRepo extends JpaRepository<Beer, Integer> {

}
