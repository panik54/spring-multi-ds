package com.springboot.multiple.datasources.bootstrap;

import com.github.javafaker.Faker;
import com.springboot.multiple.datasources.model.Beer;
import com.springboot.multiple.datasources.service.BeerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
// causes Lombok to generate a constructor with 1 parameter for each field that requires special handling.
@RequiredArgsConstructor
@Component
public class DefaultBeersLoader implements CommandLineRunner {

    private final BeerService service;
    private final Faker fake;

    // this method will be called on the application startup to add the beer data into the
    // 'beer' table present in the 'beerdb'.
    @Override
    public void run(String... args) throws Exception {
        loadBeersData();
    }

    private void loadBeersData() {
        final long count = service.getTotalBeers();
        log.info("Total beers count = {}", count);
        if (count == 0) {
            for (int x = 0; x < 5; x++) {
                service.save(createNewBeer());
            }
            log.info("Default beers are successfully saved in the database.");
        } else {
            log.info("{} beers are already present in the database.", count);
        }
    }

    private Beer createNewBeer() {
        return Beer.builder()
                .name(fake.beer().name())
                .style(fake.beer().style())
                .malt(fake.beer().malt())
                .build();
    }
}
