package com.springboot.multiple.datasources.bootstrap;

import com.github.javafaker.Faker;
import com.springboot.multiple.datasources.model.Customer;
import com.springboot.multiple.datasources.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Random;

@Slf4j
//causes Lombok to generate a constructor with 1 parameter for each field that requires special handling.
@RequiredArgsConstructor
@Component
public class DefaultCustomersLoader implements CommandLineRunner {

	private static final String[] GENDER = {"Male", "Female", "Not to specify"};
	private static final Random RANDOM = new Random();

	private final CustomerService service;
	private final Faker fake;

	// this method will be called on the application startup to add the beer data into the
    // 'customer' table present in the 'customerdb'.
	@Override
	public void run(String... args) throws Exception {
		loadCustomersData();
	}

	private void loadCustomersData() {
		final long count = service.getTotalCustomers();
		log.info("Total customers count = {}", count);
		if (count == 0) {
			for (int x = 0; x < 5; x++) {
				service.save(createNewCustomer());
			}
			log.info("Default customers are successfully saved in the database.");
		} else {
			log.info("{} customer are already present in the database.", count);
		}
	}

	private Customer createNewCustomer() {
		final LocalDate birthdate = fake.date().birthday(24, 40)
				.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		final int age = Period.between(birthdate, LocalDate.now()).getYears();
		final String gender = GENDER[RANDOM.nextInt(GENDER.length)];

		return Customer.builder()
				.fullName(fake.name().fullName())
				.age(age)
				.gender(gender)
				.phoneNumber(fake.phoneNumber().cellPhone())
				.build();
	}
}
