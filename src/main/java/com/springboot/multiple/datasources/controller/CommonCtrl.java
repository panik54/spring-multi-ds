package com.springboot.multiple.datasources.controller;

import com.springboot.multiple.datasources.model.Beer;
import com.springboot.multiple.datasources.model.Customer;
import com.springboot.multiple.datasources.service.BeerService;
import com.springboot.multiple.datasources.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
@Slf4j
public class CommonCtrl {

    @Autowired
    private BeerService service;
    @Autowired
    private CustomerService service2;

    // URL - http://localhost:8080/jcg/api/beers
    @GetMapping(value = "/beers")
    public ResponseEntity<List<Beer>> findAllBeers() {
        log.info("Fetch all beers info from the database.");
        // will fetch the beers data from the 'beer' table present in the 'beerdb'
        final List<Beer> beers = service.finalAll();
        return new ResponseEntity<>(beers, HttpStatus.OK);
    }

    // URL - http://localhost:8080/jcg/api/customers
    @GetMapping(value = "/customers")
    public ResponseEntity<List<Customer>> findAllCustomer() {
        log.info("Fetch all customers info from the database.");
        // will fetch the customers data from the 'customer' table present in the 'customerdb'
        final List<Customer> customers = service2.findAll();
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }
}
