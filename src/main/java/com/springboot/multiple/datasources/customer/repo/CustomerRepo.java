package com.springboot.multiple.datasources.customer.repo;

import com.springboot.multiple.datasources.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {

}
