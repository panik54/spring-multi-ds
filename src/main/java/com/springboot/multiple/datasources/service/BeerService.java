package com.springboot.multiple.datasources.service;

import com.springboot.multiple.datasources.beer.repo.BeerRepo;
import com.springboot.multiple.datasources.model.Beer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeerService {

    @Autowired
    private BeerRepo repo;

    public long getTotalBeers() {
        return repo.count();
    }

    public void save(final Beer beer) {
        repo.save(beer);
    }

    public List<Beer> finalAll() {
        return repo.findAll();
    }
}
