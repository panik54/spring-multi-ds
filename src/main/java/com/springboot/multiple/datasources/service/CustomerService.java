package com.springboot.multiple.datasources.service;

import com.springboot.multiple.datasources.customer.repo.CustomerRepo;
import com.springboot.multiple.datasources.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepo repo;

    public long getTotalCustomers() {
        return repo.count();
    }

    public void save(final Customer customer) {
        repo.save(customer);
    }

    public List<Customer> findAll() {
        return repo.findAll();
    }
}
